package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/handlers"
)

func PluginRoutes(group *gin.RouterGroup, e common.Env) *gin.RouterGroup {
	pluginGroup := group.Group("/plugin-discovery")

	pluginGroup.GET("", common.ConstructResponse(handlers.ListPlugins, e))

	return pluginGroup
}
